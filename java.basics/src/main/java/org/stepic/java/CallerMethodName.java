package org.stepic.java;

public class CallerMethodName {
    public static void main(String[] args) {
        System.out.println(getCallerClassAndMethodName());
        anotherMethod();
    }

    private static void anotherMethod() {
        System.out.println(getCallerClassAndMethodName());
    }

    public static String getCallerClassAndMethodName() {
        try {
            throw new RuntimeException();
        } catch (Exception e) {
            StackTraceElement[] stackTrace = e.getStackTrace();
            if (stackTrace.length < 3) return null;
            StackTraceElement stackTraceElement = stackTrace[2];
            return stackTraceElement.getClassName() + "#" + stackTraceElement.getMethodName();
        }
    }
}
