package org.stepic.java.collections;

import java.io.*;
import java.util.*;

public class Main {
    public static <T> Set<T> symmetricDifference(Set<? extends T> set1, Set<? extends T> set2) {
        Set<T> result = new HashSet<>();
        result.addAll(set1);
        result.addAll(set2);

        Iterator<T> iterator = result.iterator();
        iterator.forEachRemaining(t -> {
            if (set1.contains(t) && set2.contains(t)) {
                iterator.remove();
            }
        });


        return result;
    }

    public static void main(String[] args) {
        process(System.in, System.out);
    }

    public static void process(InputStream is, OutputStream os) {
        List<String> list = new ArrayList<>();
        try (Scanner sc = new Scanner(is)) {
            for (int i = 0; sc.hasNextInt(); i++) {
                String num = String.valueOf(sc.nextInt());
                if (i % 2 != 0) {
                    list.add(num);
                }
            }
        }
        Collections.reverse(list);
        try (PrintWriter sw = new PrintWriter(os)) {
            sw.write(String.join(" ", list));
        }
    }
}
