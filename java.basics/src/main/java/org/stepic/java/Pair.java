package org.stepic.java;

class Pair<T, V> {
    private final T t;
    private final V v;

    private Pair(T t, V v){
        this.t = t;
        this.v = v;
    };

    public static <T, V> Pair<T, V> of(T t, V v) {
        return new Pair<T, V>(t, v);
    }

    public static void main(String[] args) {
        Pair<Integer, String> pair = Pair.of(1, "hello");
        Integer i = pair.getFirst(); // 1
        String s = pair.getSecond(); // "hello"

        Pair<Integer, String> pair2 = Pair.of(1, "hello");
        boolean mustBeTrue = pair.equals(pair2); // true!
        if (!mustBeTrue) throw new IllegalStateException();
        boolean mustAlsoBeTrue = pair.hashCode() == pair2.hashCode(); // true!
        if (!mustAlsoBeTrue) throw new IllegalStateException();
    }

    private V getSecond() {
        return v;
    }

    private T getFirst() {
        return t;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pair<?, ?> pair = (Pair<?, ?>) o;

        if (t != null ? !t.equals(pair.t) : pair.t != null) return false;
        return !(v != null ? !v.equals(pair.v) : pair.v != null);

    }


    @Override
    public int hashCode() {
        int result = t != null ? t.hashCode() : 0;
        result = 31 * result + (v != null ? v.hashCode() : 0);
        return result;
    }
}
