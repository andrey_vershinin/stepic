package org.stepic.java;

import java.math.BigInteger;
import java.util.LinkedHashMap;
import java.util.Map;

public class ControlConstructs {
    public static BigInteger factorial(int value) {
        BigInteger result = BigInteger.ONE;
        for (int i = 1; i <= value; i++) {
            result = result.multiply(BigInteger.valueOf(i));
        }
        return result;
    }

    public static int[] mergeArrays(int[] a1, int[] a2) {
        int[] result = new int[a1.length + a2.length];
        int i = 0, j = 0;
        for (; i < a1.length && j < a2.length; ) {
            if (a1[i] < a2[j]) {
                result[i + j] = a1[i];
                i++;
            } else {
                result[i + j] = a2[j];
                j++;
            }
        }

        if (i < a1.length) {
            System.arraycopy(a1, i, result, i + j, a1.length - i);
        } else if (j < a2.length) {
            System.arraycopy(a2, j, result, i + j, a2.length - j);
        }

        return result;
    }

    public static String printTextPerRole(String[] roles, String[] textLines) {
        Map<String, StringBuilder> result = new LinkedHashMap<>();
        for (String role : roles) {
            result.put(role, new StringBuilder());
        }

        for (int i = 0; i < textLines.length; i++) {
            String line = textLines[i];
            String role = line.substring(0, line.indexOf(":"));
            String text = line.substring(line.indexOf(":") + 1);
            result.get(role).append(i + 1).append(")").append(text).append("\n");
        }

        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, StringBuilder> entry : result.entrySet()) {
            sb.append(entry.getKey()).append(":\n").append(entry.getValue()).append("\n");
        }
        return sb.delete(sb.length()-2, sb.length()).toString();
    }
}
