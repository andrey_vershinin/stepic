package org.stepic.java;

import java.util.function.DoubleUnaryOperator;

public class Integrator {
    public static final double PRECISION = 1.0e-4;

    private static final double DEFAULT_STEP = 1.0e-2;
    private static final double MIN_STEP = 1.0e-6;

    public static double integrate(DoubleUnaryOperator f, double a, double b) {
        double step = DEFAULT_STEP;
        double result = 0;
        while (step > MIN_STEP) {
            double prevResult = result;
            result = integrate(f, a, b, step);
            if (Double.compare(Math.abs(result - prevResult), step * step) > 0) {
                step /= 2;
            } else {
                break;
            }
        }
        return result;
    }

    private static double integrate(DoubleUnaryOperator f, double a, double b, double step) {
        double result = 0;
        while (a < b) {
            result += f.applyAsDouble(a) * step;
            a += step;
        }

        // last step
        if (Double.compare(Math.abs(b - a), Math.sqrt(PRECISION)) > 0) {
            a -= step;
            result += f.applyAsDouble(a) * (b - a);
        }
        return result;
    }
}
