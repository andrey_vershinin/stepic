package org.stepic.java.mail;

public interface Sendable {
    String getFrom();
    String getTo();
}
