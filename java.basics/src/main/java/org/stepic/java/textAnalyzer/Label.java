package org.stepic.java.textAnalyzer;

public enum Label {
    SPAM, NEGATIVE_TEXT, TOO_LONG, OK
}
