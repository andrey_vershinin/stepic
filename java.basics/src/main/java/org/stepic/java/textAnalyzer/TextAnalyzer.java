package org.stepic.java.textAnalyzer;

public interface TextAnalyzer {
    Label processText(String text);
}
