package org.stepic.java;

/**
 * Created by Midia on 18/11/15.
 */
public class AsciiCharSequence implements CharSequence {
    private final byte[] sequence;

    public AsciiCharSequence(byte[] sequence) {
        this.sequence = sequence;
    }

    @Override
    public int length() {
        return sequence.length;
    }

    @Override
    public char charAt(int index) {
        return (char) sequence[index];
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        byte[] newSequence = new byte[end - start];
        System.arraycopy(sequence, start, newSequence, 0, end - start);
        return new AsciiCharSequence(newSequence);
    }

    @Override
    public String toString() {
        return new StringBuilder(this).toString();
    }
}
