package org.stepic.java;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class ISChecksum {
    public static int checkSumOfStream(InputStream inputStream) throws IOException {
        byte[] buffer = new byte[1024];
        int read, result = 0;
        while ((read = inputStream.read(buffer)) > 0) {
            for (int i = 0; i < read; i++) {
                result = Integer.rotateLeft(result, 1) ^ (buffer[i] & 0xFF);
            }
        }
        return result;
    }

    public static void main(String[] args) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try (PrintWriter pw = new PrintWriter(new OutputStreamWriter(os, StandardCharsets.UTF_8))) {
            pw.print('Ы');
        }
        for (byte b : os.toByteArray()) {
            System.out.println(b & 0xFF);
        }
    }
}
