package org.stepic.java.doubles;

import java.io.InputStream;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.print(sumDoubles(System.in));
    }

    public static String sumDoubles(InputStream in) {
        double result = 0;
        try (Scanner sc = new Scanner(in)) {
            while (sc.hasNext()) {
                try {
                    result += Double.parseDouble(sc.next());
                } catch (NumberFormatException e) { /* ignore */ }
            }
        }
        return String.format("%.6f", result);
    }
}
