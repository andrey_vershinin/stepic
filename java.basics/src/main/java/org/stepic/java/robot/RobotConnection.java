package org.stepic.java.robot;

public interface RobotConnection extends AutoCloseable {
    void moveRobotTo(int x, int y);
    void close();
}
