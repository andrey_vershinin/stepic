package org.stepic.java.robot;

import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.XMLFormatter;

public class App {
    public static final int RETRIES = 3;

    public static void moveRobot(RobotConnectionManager robotConnectionManager, int toX, int toY) {
        RobotConnectionException lastException = null;
        for (int i = 0; i < RETRIES; i++) {
            RobotConnection connection = null;
            try {
                connection = robotConnectionManager.getConnection();
                connection.moveRobotTo(toX, toY);
                lastException = null;
                break;
            } catch (RobotConnectionException e) {
                lastException = e;
                System.out.println("Failed to connect, retrying " + (i+1) + " time");
            } finally {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (Exception e1) {
                        if (lastException != null) {
                            lastException.addSuppressed(e1);
                        } else {
                            System.out.println("WARN: error closing connection");
                            e1.printStackTrace();
                        }
                    }
                }
            }
        }
        if (lastException != null) {
            throw lastException;
        }
    }

    public static void main(String[] args) {
        Logger.getLogger("org.stepic.java.logging.ClassA").setLevel(Level.ALL);
        Logger.getLogger("org.stepic.java.logging.ClassB").setLevel(Level.WARNING);
        ConsoleHandler consoleHandler = new ConsoleHandler();
        consoleHandler.setFormatter(new XMLFormatter());
        Logger root = Logger.getLogger("org.stepic");
        root.setUseParentHandlers(false);
        root.setLevel(Level.ALL);
        root.addHandler(consoleHandler);
    }
}
