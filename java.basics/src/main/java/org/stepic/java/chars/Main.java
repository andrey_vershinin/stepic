package org.stepic.java.chars;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;

public class Main {
    public static void main(String[] args) throws Exception {
        InputStream in = System.in;
        byte[] buffer = new byte[1024];
        int read, totalRead = 0, lastIndexOfR = -1;
        while ((read = in.read(buffer)) > 0) {
            for (int i = 0; i < read; i++) {
                if (buffer[i] != 10 && totalRead == lastIndexOfR) {
                    System.out.write(13);
                }
                totalRead++;
                if (buffer[i] == 13) {
                    lastIndexOfR = totalRead;
                } else {
                    System.out.write(buffer[i]);
                }
            }
        }
        System.out.flush();
    }

    public static String readAsString(InputStream inputStream, Charset charset) throws IOException {
        StringBuilder sw = new StringBuilder();
        Reader isr = new InputStreamReader(inputStream, charset);
        int read = 0;
        char[] buffer = new char[512];
        while((read = isr.read(buffer)) > 0){
            for (int i = 0; i < read; i++) {
                sw.append(buffer[i]);
            }
        }
        return sw.toString();
    }
}
