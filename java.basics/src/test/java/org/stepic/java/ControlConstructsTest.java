package org.stepic.java;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Midia on 17/11/15.
 */
public class ControlConstructsTest {

    @Test
    public void testMergeArrays() throws Exception {
        assertArrayEquals(new int[0], ControlConstructs.mergeArrays(new int[0], new int[0]));
        assertArrayEquals(new int[]{1, 2}, ControlConstructs.mergeArrays(new int[]{1}, new int[]{2}));
        assertArrayEquals(new int[]{1, 2, 3}, ControlConstructs.mergeArrays(new int[]{1, 2}, new int[]{3}));
        assertArrayEquals(new int[]{1, 2, 3}, ControlConstructs.mergeArrays(new int[]{1}, new int[]{2, 3}));
        assertArrayEquals(new int[]{1, 2, 3, 4}, ControlConstructs.mergeArrays(new int[]{1, 3}, new int[]{2, 4}));
    }

    @Test
    public void testOpera() throws Exception {
        assertEquals("Городничий:\n" +
                "1) Я пригласил вас, господа, с тем, чтобы сообщить вам пренеприятное известие: к нам едет ревизор.\n" +
                "4) Ревизор из Петербурга, инкогнито. И еще с секретным предписаньем.\n" +
                "\n" +
                "Аммос Федорович:\n" +
                "2) Как ревизор?\n" +
                "5) Вот те на!\n" +
                "\n" +
                "Артемий Филиппович:\n" +
                "3) Как ревизор?\n" +
                "6) Вот не было заботы, так подай!\n" +
                "\n" +
                "Лука Лукич:\n" +
                "7) Господи боже! еще и с секретным предписаньем!",
                ControlConstructs.printTextPerRole(
                        new String[]{"Городничий","Аммос Федорович", "Артемий Филиппович", "Лука Лукич"},
                        new String[]{
                                "Городничий: Я пригласил вас, господа, с тем, чтобы сообщить вам пренеприятное известие: к нам едет ревизор.",
                                "Аммос Федорович: Как ревизор?",
                                "Артемий Филиппович: Как ревизор?",
                                "Городничий: Ревизор из Петербурга, инкогнито. И еще с секретным предписаньем.",
                                "Аммос Федорович: Вот те на!",
                                "Артемий Филиппович: Вот не было заботы, так подай!",
                                "Лука Лукич: Господи боже! еще и с секретным предписаньем!"
                        }
                )
                );
    }
}