package org.stepic.java;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.stepic.java.Integrator.*;

public class IntegratorTest {
    @Test
    public void testIntegrate() throws Exception {
        assertEquals(1.0, integrate(x -> 1, 0, 1), PRECISION);
        assertEquals(0.5, integrate(x -> x, 0, 1), PRECISION);
        assertEquals(1.0/3, integrate(x -> x*x, 0, 1), PRECISION);
    }
}