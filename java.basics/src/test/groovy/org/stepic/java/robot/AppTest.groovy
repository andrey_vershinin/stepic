package org.stepic.java.robot

import spock.lang.*

class AppTest extends Specification {


    def "Failed 3 connection attampts"() {
        setup:
        def cm = Mock(RobotConnectionManager) {
            3 * getConnection() >> { throw new RobotConnectionException("") }
        }
        when:
        App.moveRobot(cm, 1, 1)
        then:
        thrown(RobotConnectionException)
    }

    def "Failed 3 send attampts"() {
        setup:
        def conn = Mock(RobotConnection) {
            3 * moveRobotTo(_, _) >> { throw new RobotConnectionException("") }
        }
        def cm = Mock(RobotConnectionManager) {
            getConnection() >> conn
        }
        when:
        App.moveRobot(cm, 1, 1)
        then:
        thrown(RobotConnectionException)
    }

    def "Succeeded after 1 connection obtaining and 1 send attampts"() {
        setup:
        def conn = Mock(RobotConnection) {
            def count = 0
            2 * moveRobotTo(_, _) >> { if (count++ < 1) throw new RobotConnectionException("") }
        }

        def cm = Mock(RobotConnectionManager) {
            3 * getConnection() >> { throw new RobotConnectionException("") } >>> conn
        }
        when:
        App.moveRobot(cm, 1, 1)
        then:
        noExceptionThrown()
    }

    def "Succeeded immediately"() {
        setup:
        def conn = Mock(RobotConnection) {
            def count = 0
            2 * moveRobotTo(_, _) >> { if (count++ < 1) throw new RobotConnectionException("") }
        }

        def cm = Mock(RobotConnectionManager) {
            3 * getConnection() >> { throw new RobotConnectionException("") } >>> conn
        }
        when:
        App.moveRobot(cm, 1, 1)
        then:
        noExceptionThrown()
    }

}
