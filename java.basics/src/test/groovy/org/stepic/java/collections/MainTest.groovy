package org.stepic.java.collections

import spock.lang.Specification

class MainTest extends Specification {
    def "test symmetricDifference"(Set<?> s1, Set<?> s2, Set<?> s3) {
        expect:
        Main.symmetricDifference(s1, s2).equals(s3)
        where:
        s1               | s2               | s3
        newHashSet(1, 2) | newHashSet(2, 3) | newHashSet(1, 3)
        newHashSet(1)    | newHashSet(1)    | newHashSet()
        newHashSet(1)    | newHashSet(2)    | newHashSet(1, 2)
    }

    def newHashSet(int... args) {
        return new HashSet<>(Arrays.asList(args))
    }

    def "Even-position numbers removed and reversed list returned"(String i, String o) {
        given:
            ByteArrayInputStream is = new ByteArrayInputStream(i.getBytes());
            ByteArrayOutputStream os = new ByteArrayOutputStream()
            Main.process(is, os);
        expect:
            os.toString().equals(o)
        where:
            i                   |   o
            "1 2 3 4 5 6 7"     |   "6 4 2"
            ""                  |   ""
    }

}
