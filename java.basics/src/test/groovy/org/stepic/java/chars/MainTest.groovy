package org.stepic.java.chars

import org.stepic.java.chars.Main
import spock.lang.Specification

import java.nio.charset.StandardCharsets

class MainTest extends Specification {
    def "test readAsString"() {
        def buf = [48, 49, 50, 51] as byte[]
        def bis = new ByteArrayInputStream(buf);
        expect:
            Main.readAsString(bis, StandardCharsets.US_ASCII).equals("0123")
    }
}
