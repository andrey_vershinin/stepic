package org.stepic.java.function

import spock.lang.Specification

class TernaryTest extends Specification {
    def "test length"(String s, int length) {
        expect:
            Ternary.length(s) == length
        where:
            s       |  length
            "123"   |   3
            null    |   0
            ""      |   0
    }
}
