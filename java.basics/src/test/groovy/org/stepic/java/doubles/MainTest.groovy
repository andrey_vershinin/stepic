package org.stepic.java.doubles

import spock.lang.Specification

class MainTest extends Specification {
    def "Sums doubles correctly"(String input, String result) {
        expect:
            Main.sumDoubles(new ByteArrayInputStream(input.bytes)).equals(result)
        where:
            input                   | result
            "1 2 3"                 | "6.000000"
            "a1 b2 c3"              | "0.000000"
            "-1e3\n18 .111 11bbb"   | "-981.889000"
    }
}
