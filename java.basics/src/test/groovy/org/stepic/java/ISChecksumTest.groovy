package org.stepic.java

import spock.lang.Specification

class ISChecksumTest extends Specification {
    def "Returns 71 for 0x33 0x45 0x01"() {
        def buf = [0x33, 0x45, 0x01 ] as byte[]
        expect:
            ISChecksum.checkSumOfStream(new ByteArrayInputStream(buf)) == 71
    }
}
